# coding: utf-8
lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "unbounceauto/version"

Gem::Specification.new do |spec|
  spec.name          = "unbounceauto"
  spec.version       = Unbounceauto::VERSION
  spec.authors       = ["James Coles-Nash"]
  spec.email         = ["mail@jcnash.com"]

  spec.summary       = "A experimental hiring project for Unbounce"
  spec.homepage      = "https://bitbucket.org/jcolesnash/unbounceauto"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata["allowed_push_host"] = "TODO: Set to 'http://mygemserver.com'"
  else
    raise "RubyGems 2.0 or newer is required to protect against " \
      "public gem pushes."
  end

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.15"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec", "~> 3.0"
  spec.add_development_dependency "selenium-webdriver", "~> 3.4.3"
  spec.add_development_dependency "page-object", "~> 2.1.1"
  spec.add_development_dependency "pry", "~> 0.10.4"
  spec.add_development_dependency "require_all", "~> 1.4.0"
end

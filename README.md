# Unbounceauto

An experimental hiring project for Unbounce

## Requirements

[Google chrome](https://www.google.com/chrome/browser/desktop/index.html)
[Google chrome driver](https://sites.google.com/a/chromium.org/chromedriver/)

## Usage

```
#!bash
git clone git@bitbucket.org:jcolesnash/unbounceauto.git
bundle install
```

Update spec/spec_helper.rb line 21 and 24 to the paths of chrome and chrome driver.

```
#!bash
rspec spec/register_spec.rb
```
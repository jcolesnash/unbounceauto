class RootPage
    include PageObject

    page_url "https://unbounce.com/"
    link(:start_free_trial_link, css: "#menu-top-buttons-1 li.top-btn.btn.menu-start-free-trial a")
end
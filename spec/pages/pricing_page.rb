class PricingPage
    include PageObject

    page_url "https://unbounce.com/pricing/"
    link(:annual_start_link, css: "a[href='https://app.unbounce.com/signup/essential_2017_annual']")
    link(:annual_monthly_toggle_link, id: "pricing-plan")
end
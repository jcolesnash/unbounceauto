class RegisterPage
    include PageObject

    page_url "https://app.unbounce.com/registrations/${}/edit"

    labels(:invalid_labels, css: "label.invalid")

    # Account Info
    text_field(:first_name_textfield, id: "registration_first_name")
    label(:first_name_validation_label, css: "label[for='registration_first_name']")

    text_field(:last_name_textfield, id: "registration_last_name")
    label(:last_name_validation_label, css: "label[for='registration_last_name']")

    text_field(:email_textfield, id: "registration_email")
    label(:email_validation_label, css: "label[for='registration_email']")

    text_field(:password_textfield, id: "registration_password")
    label(:password_validation_label, css: "label[for='registration_password']")
    paragraph(:password_validation_message_paragraph, css: "label[for='registration_password'] ~ p")

    button(:submit_button, css: "#account-info > button")
    checkbox(:mailing_list_checkbox, id: "registration_mailing_list_optin")

    # Billing Info
    h2(:billing_info_h2, css: "#billing-info h2")
end
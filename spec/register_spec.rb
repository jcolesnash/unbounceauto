require "spec_helper"

def wait_for_validations
  @register_page.wait_until(5, "Validation did not appear") do
    @register_page.invalid_labels_elements.count > 0
  end
end

def wait_for_billing_info
  @register_page.wait_until(5, "Billing info did not appear") do
    @register_page.billing_info_h2.include?("Billing Information")
  end
end

RSpec.describe Unbounceauto do
  describe "Register" do
    before(:each) do
      @pricing_page.goto
    end

    describe "Validation" do
      before(:each) do
        @pricing_page.annual_start_link
      end

      it "All fields" do
        @register_page.submit_button

        wait_for_validations

        expect(@register_page.first_name_validation_label_element.attribute('class')).to include("invalid")
        expect(@register_page.last_name_validation_label_element.attribute('class')).to include("invalid")
        expect(@register_page.email_validation_label_element.attribute('class')).to include("invalid")
        expect(@register_page.password_validation_label_element.attribute('class')).to include("invalid")
      end

      it "Short Password" do
        @register_page.first_name_textfield = "Han"
        @register_page.last_name_textfield = "Solo"
        @register_page.email_textfield = "Han.Solo@example.com"
        @register_page.password_textfield = "Wookie"

        @register_page.submit_button
        
        wait_for_validations

        expect(@register_page.first_name_validation_label_element.attribute('class')).not_to include("invalid")
        expect(@register_page.last_name_validation_label_element.attribute('class')).not_to include("invalid")
        expect(@register_page.email_validation_label_element.attribute('class')).not_to include("invalid")
        expect(@register_page.password_validation_label_element.attribute('class')).to include("invalid")

        expect(@register_page.password_validation_message_paragraph).to include("Enter at least 8 characters")
      end

      it "Valid proceeds to billing info" do
        @register_page.first_name_textfield = "Han"
        @register_page.last_name_textfield = "Solo"
        @register_page.email_textfield = "Han.Solo@example.com"
        @register_page.password_textfield = "WookieLegs"

        @register_page.submit_button
        
        wait_for_billing_info

        expect(@register_page.billing_info_h2).to include("Billing Information") 
      end
    end
  end
end

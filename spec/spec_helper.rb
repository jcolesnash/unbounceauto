require "bundler/setup"
require "unbounceauto"
require 'selenium-webdriver'
require "page-object"
require "require_all"
require "pry"

require_all 'spec/pages/'

include Selenium

RSpec.configure do |config|
  config.example_status_persistence_file_path = ".rspec_status"
  config.disable_monkey_patching!
  config.expect_with :rspec do |c|
    c.syntax = :expect
  end

  # Selenium
  config.before(:all) do
    Selenium::WebDriver::Chrome.driver_path="c:/chromedriver/chromedriver.exe"
    caps = Selenium::WebDriver::Remote::Capabilities.chrome(                       
      "chromeOptions" => {                                                         
          'binary' => "C:/Program Files (x86)/Google/Chrome/Application/chrome.exe",           
          'args' => %w{headless disable-gpu}                              
        }                                                                            
      )                                                                              
    @driver = Selenium::WebDriver.for :chrome, desired_capabilities: caps

    # Pages
    @root_page = RootPage.new(@driver)
    @register_page = RegisterPage.new(@driver)
    @pricing_page = PricingPage.new(@driver)
  end

  config.after(:all) do
    @driver.close
    @driver.quit
  end
end
